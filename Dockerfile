FROM node:11-alpine
RUN mkdir -p /app
WORKDIR /app
COPY package.json .
RUN yarn
COPY . .
EXPOSE 4000
CMD ["yarn", "start"]
