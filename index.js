const Koa = require('koa')
const Router = require('koa-router')
const logger = require('koa-logger')
const bodyParser = require('koa-bodyparser')

const os = require('os')

const Influx = require('influx')

const app = new Koa()
const router = new Router()

// const open = require('./open')

const influx = new Influx.InfluxDB({
  database: 'shapes-access',
  host: 'influxdb.cloud.100shapes.com',
  port: 8086,
  username: 'shapes-access',
  password: process.env.PASSWORD,
  schema: [{
    measurement: 'access',
    fields: {
      type: Influx.FieldType.STRING,
    },
    tags: [
      'person'
    ]
  }]
})


router.get('/', async ctx => {
  ctx.body = "Open door"
  console.log(ctx.body)
})


router.post('/', async ctx => {
  let {access} = ctx.request.body

  let response = await influx.writePoints([
    {
      measurement: 'access',
      tags: { person: person },
      fields: { type: "Fob"},
    }
  ])
  console.log(response)
})





app.use(bodyParser())
app.use(logger())
app.use(router.routes())
app.use(router.allowedMethods())

app.listen(3000)
